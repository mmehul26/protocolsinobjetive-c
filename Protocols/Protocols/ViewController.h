//
//  ViewController.h
//  Protocols
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildViewController.h"

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;


@end

