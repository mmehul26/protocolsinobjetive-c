//
//  ChildViewController.m
//  Protocols
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "ChildViewController.h"

@interface ChildViewController ()

@end

@implementation ChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [self.delegate receiveMessage:self.nameTextFld.text];
}
@end
