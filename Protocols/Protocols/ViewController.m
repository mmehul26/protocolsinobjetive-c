//
//  ViewController.m
//  Protocols
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "ViewController.h"
#import "ChildViewController.h"

@interface ViewController () <FirstVCDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    self.nameLabel.text = searchStr;
    
    return true;
}
// Before we present the Second View Controller we have to declare it's delegate to be the First View Controller
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ChildViewController *secondVC = [segue destinationViewController];
    ViewController *firstVC = [segue sourceViewController];
    secondVC.delegate = firstVC;
}

// This will just update the label text with the message we get from the Second View Controller
- (void)receiveMessage:(NSString *)message {
    self.nameLabel.text = message;
}
@end
