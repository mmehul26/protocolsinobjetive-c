//
//  AppDelegate.h
//  Protocols
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

