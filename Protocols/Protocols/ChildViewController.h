//
//  ChildViewController.h
//  Protocols
//
//  Created by Mehul Makwana on 31/08/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ViewController.h"

@protocol FirstVCDelegate <NSObject>

@required
- (void)receiveMessage:(NSString *)message;

@end

@interface ChildViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *nameTextFld;

@property (weak, nonatomic) id<FirstVCDelegate> delegate;


@end
